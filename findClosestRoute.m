function [min_cost_fidx min_cost_bidx] = findClosestRoute(frontier_node_f, ...
    frontier_dist_f, frontier_node_b, frontier_dist_b)
%FINDCLOSESTROUTE Given frontier node and distance, check the closet route
%   shared here
%   @author Jing Dong

% get common node idx
common_node_idx_list = [];
for i=1:numel(frontier_node_f)
    if sum(find(frontier_node_f(i) == frontier_node_b)) ~= 0
        common_node_this = find(frontier_node_f(i) == frontier_node_b);
        for j=1:numel(common_node_this)
            common_node_idx_list = [common_node_idx_list; [i common_node_this(j)]];
        end
    end
end

% connect route, add cost
for i=1:size(common_node_idx_list, 1)
    cost_list(i) = frontier_dist_f(common_node_idx_list(i, 1)) + ...
        frontier_dist_b(common_node_idx_list(i, 2));
end
min_cost_route_idx = find(cost_list == min(cost_list));
min_cost_route_idx = min_cost_route_idx(1);
min_cost_fidx = common_node_idx_list(min_cost_route_idx, 1);
min_cost_bidx = common_node_idx_list(min_cost_route_idx, 2);

end

