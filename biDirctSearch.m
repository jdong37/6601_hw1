function result = biDirctSearch(graph, root, goal)
%BIDIRCTSEARCH Bi-Direction Search for two nodes 
%   @author Jing Dong

% init: forward and bakcward
frontier_node_f = [root];
frontier_dist_f = [0];
frontier_node_b = [goal];
frontier_dist_b = [0];

route_f = cell(1,1);
route_f{1} = [root];
route_b = cell(1,1);
route_b{1} = [goal];

explored_f = [];
explored_b = [];

lowest_cost_idx_f = 1;
lowest_cost_node_f = root;
lowest_cost_idx_b = 1;
lowest_cost_node_b = goal;

% loop
while ~(numel(frontier_node_f) == 0 || lowest_cost_node_f == goal || ...
        numel(frontier_node_b) == 0 || lowest_cost_node_b == root || ...
        findCommon(frontier_node_f, frontier_node_b))
    
    % call usearchEngine: forward and bakcward
    [frontier_node_f, frontier_dist_f, route_f, ...
        explored_f, lowest_cost_idx_f, lowest_cost_node_f] = ...
        usearchEngine(frontier_node_f, frontier_dist_f, route_f, ...
        explored_f, lowest_cost_idx_f, lowest_cost_node_f, graph);

    [frontier_node_b, frontier_dist_b, route_b, ...
        explored_b, lowest_cost_idx_b, lowest_cost_node_b] = ...
        usearchEngine(frontier_node_b, frontier_dist_b, route_b, ...
        explored_b, lowest_cost_idx_b, lowest_cost_node_b, graph);

end

% check whether frontier is empty: empty means cannot reach goal
if numel(frontier_node_f) == 0 || numel(frontier_node_b) == 0
    result.is_found = 0;
    return
end

% check whether both side meet each other
if findCommon(frontier_node_f, frontier_node_b)
    % get closet route
    [min_cost_fidx min_cost_bidx] = findClosestRoute(frontier_node_f, ...
        frontier_dist_f, frontier_node_b, frontier_dist_b);
    % get result
    result.is_found = 1;
    result.cost = frontier_dist_f(min_cost_fidx) + frontier_dist_b(min_cost_bidx);
    result.route = [route_f{min_cost_fidx} wrev(route_b{min_cost_bidx})];
    result.explored_node{1} = explored_f;
    result.explored_node{2} = explored_b;
    return
end

% check whether in a single side goal is reached
if lowest_cost_node_f == goal
    result.is_found = 1;
    result.cost = frontier_dist_f(lowest_cost_idx_f);
    result.route = route_f{lowest_cost_idx_f};
    result.explored_node{1} = explored_f;
    result.explored_node{2} = explored_b;
    return
end

if lowest_cost_node_b == root
    result.is_found = 1;
    result.cost = frontier_dist_b(lowest_cost_idx_b);
    result.route = route_b{lowest_cost_idx_b};
    result.explored_node{1} = explored_f;
    result.explored_node{2} = explored_b;
    return
end

end



