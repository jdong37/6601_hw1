function plotRoute(graph, route, fig, color_str, param_str, param)
%PLOTROUTE plot found route
%   @author Jing Dong

figure(fig)
hold on

% node corrdinate
node_vec_x = zeros(1, numel(route));
node_vec_y = zeros(1, numel(route));
for i=1:numel(route)
    node_vec_x(i) = graph.node{route(i)}.xy(1);
    node_vec_y(i) = graph.node{route(i)}.xy(2);
end

% plot trajory
plot(node_vec_x, node_vec_y, color_str, param_str, param);

hold off

end

