function graph = parseOSM2Graph(osm_map, ref_point)
%PARSEOSM2GRAPH Parse OSM output to graph strcuture
%   @author Jing Dong

% graph data struct:
%   node: node cell
%       node{i}.xy: position vector [x y]
%           x, y are in meters, ref point indicate by input arg
%   dist_mat: distance matrix, use distance, inf means not connect
%   connect_dirct: connectivity tree

disp('loading from OSM object ...');

way = osm_map.way;
node = osm_map.node;

% =========================================================================
% scan each way, read all tag.Attributes, find 'highway to put in the graph'
way_flag_highway = zeros(1, size(way.id, 2));
for i=1:size(way.id, 2)
%     fprintf('way #: %d\n', i)
    
    flag_highway = 0;
    % find tag.Attributes.k == 'highway'
    tag_size = size(way.tag{i}, 2);
%     fprintf('tag size: %d\n', tag_size)
    if tag_size > 0
        if tag_size == 1
            % only one tag: should == highway
            if strcmp(way.tag{i}.Attributes.k, 'highway') ~= 0
                flag_highway = 1;
            end
        else
            % several tag: find highway in any one
            for j=1:size(way.tag{i}, 2)
                if strcmp(way.tag{i}{j}.Attributes.k, 'highway') ~= 0
                    flag_highway = 1;
                    break;
                end
            end
        end
    end
    
    if flag_highway
%         disp('YES highway')
    else
%         disp('NO highway')
    end
    way_flag_highway(1, i) = flag_highway;
    
%     disp('==============================')
end

% =========================================================================
% put in the graph

% prepare node
% disp('preparing nodes ...');

% count valid node
valid_node_idx = [];
for i=1:size(way.id, 2)
    if way_flag_highway(i)
        for j=1:size(way.nd{i}, 2)
            if sum(find(valid_node_idx == way.nd{i}(j))) == 0
                valid_node_idx = [valid_node_idx way.nd{i}(j)];
            end
        end
    end
end

graph.node = cell(1,numel(valid_node_idx));
for i=1:numel(valid_node_idx)
    node_count = find(node.id == valid_node_idx(i));
    single_node.xy = diff_latlog(node.xy(:, node_count)', ref_point) ;
    graph.node{i} = single_node;
end

% prepare node matrix
% disp('preparing connectivity ...');

graph.dist_mat = Inf(numel(valid_node_idx));
graph.connect_dirct = cell(1, numel(valid_node_idx));
for i=1:size(way.id, 2)
    if way_flag_highway(i)
        for j=1:(size(way.nd{i}, 2) - 1)
            
            % get node index
            node_count1 = find(valid_node_idx == way.nd{i}(j));
            node_count2 = find(valid_node_idx == way.nd{i}(j+1));
            if sum(node_count1) + sum(node_count2) == 0
                error('call not exist node!');
            end
            % put in connectivity
            if (sum(find(graph.connect_dirct{node_count1} == node_count2)) == 0)
                graph.connect_dirct{node_count1} = [graph.connect_dirct{node_count1} node_count2];
            end
            % put in matrix
            graph.dist_mat(node_count1, node_count2) = ...
                norm(graph.node{node_count1}.xy - graph.node{node_count2}.xy);
            
            % check one-way
            flag_oneway = 0;
            tag_size = size(way.tag{i}, 2);
            if tag_size > 1
                % several tag: find highway in any one
                for j=1:size(way.tag{i}, 2)
                    if strcmp(way.tag{i}{j}.Attributes.k, 'oneway') ~= 0
%                         fprintf('idx = %d\n', i)
%                         disp('oneway detected');
                        if strcmp(way.tag{i}{j}.Attributes.v, 'yes') ~= 0
%                             disp('oneway found');
                            flag_oneway = 1;
                        end
                        break;
                    end
                end
            end
            
            % not oneway, put in reverse way
            if flag_oneway == 0;
                % put in connectivity
                if (sum(find(graph.connect_dirct{node_count2} == node_count1)) == 0)
                    graph.connect_dirct{node_count2} = [graph.connect_dirct{node_count2} node_count1];
                end
                % put in matrix
                graph.dist_mat(node_count2, node_count1) = ...
                    norm(graph.node{node_count1}.xy - graph.node{node_count2}.xy);
            end
        end
    end
end

end

% =========================================================================
% relative point, second point as ref
function point = diff_latlog(n1_latlog, n2_latlog)

RADIUS_EARTH = 6371000;
    
long_diff = n1_latlog(1) - n2_latlog(1);
lat_diff = n1_latlog(2) - n2_latlog(2);
x = long_diff * pi/180 * cosd(n2_latlog(2)) * RADIUS_EARTH;
y = lat_diff * pi/180 * RADIUS_EARTH;
point = [x y];

end


