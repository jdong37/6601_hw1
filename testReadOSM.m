% test read OSM map in
% @author Jing Dong

clear
close all
includeDependencies;
addpath('./data')

% load from OSM
% openstreetmap_filename = 'atlanta.osm';
% [parsed_osm, osm_xml] = parse_openstreetmap(openstreetmap_filename);
load('atlanta_osm.mat');

% use OSM utils to plot
% fig = figure;
% ax = axes('Parent', fig);
% hold(ax, 'on')
% plot_way(ax, parsed_osm)
% hold(ax, 'off')

% parse data to graph format
ref_point = [-84.37 33.79];
graph = parseOSM2Graph(parsed_osm, ref_point);

% plot
disp('ploting ...')
plotGraph(graph, 2);
