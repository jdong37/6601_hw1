function plotGraph(graph, fig)
%PLOTGRAPH plot the graph
%   @author Jing Dong

figure(fig)
hold on

% node corrdinate
node_vec_x = zeros(1, size(graph.node, 2));
node_vec_y = zeros(1, size(graph.node, 2));
for i=1:size(graph.node, 2)
    node_vec_x(i) = graph.node{i}.xy(1);
    node_vec_y(i) = graph.node{i}.xy(2);
end

% plot edges
for i=1:size(graph.connect_dirct, 2)
    connect_size = size(graph.connect_dirct{i}, 2);
    for j=1:connect_size
        edge_vec_x = zeros(1, 2);
        edge_vec_y = zeros(1, 2);
        edge_vec_x(1) = graph.node{i}.xy(1);
        edge_vec_y(1) = graph.node{i}.xy(2);
        edge_vec_x(2) = graph.node{graph.connect_dirct{i}(j)}.xy(1);
        edge_vec_y(2) = graph.node{graph.connect_dirct{i}(j)}.xy(2);
        % plot oneway in cyan, not oneway in blue
        if isinf(max(graph.dist_mat(i, graph.connect_dirct{i}(j)), ...
                graph.dist_mat(graph.connect_dirct{i}(j), i)))
%             plot(edge_vec_x, edge_vec_y, 'c-');
            plot(edge_vec_x, edge_vec_y, 'b-');
        else
            plot(edge_vec_x, edge_vec_y, 'b-');
        end
    end
end

% plot node
% plot(node_vec_x, node_vec_y, 'k.', 'MarkerSize', 1);

hold off

end

