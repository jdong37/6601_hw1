% prepare test graph graph_test for unit test purpose

% node
node = [0 0;3 3;5 0;6 2;4 6];
graph_test.node = cell(1,size(node, 1));
for i=1:size(node, 1)  
    graph_test.node{i}.xy = node(i,:);
end

% connect
graph_test.connect_dirct = cell(1,size(node, 1));
graph_test.connect_dirct{1} = [2 3];
graph_test.connect_dirct{2} = [1 3 5];
graph_test.connect_dirct{3} = [1 2 4];
graph_test.connect_dirct{4} = [3 5];
graph_test.connect_dirct{5} = [2 4];

% dist matrix
graph_test.dist_mat = Inf(5);
for i=1:5
    for j=1:5
        if sum(find(graph_test.connect_dirct{i} == j)) ~= 0
            graph_test.dist_mat(i,j) = norm(node(i,:) - node(j,:));
        end
    end
end

% graph_test.dist_mat
% plotGraph(graph_test, 1001);