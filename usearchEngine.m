function [frontier_node_list, frontier_dist_list, route_list, ...
    explored_list, lowest_cost_idx, lowest_cost_node] = ...
    usearchEngine(frontier_node_list, frontier_dist_list, route_list, explored_list, ...
    lowest_cost_idx, lowest_cost_node, graph)
%USEARCHENGINE uniform search engine
%   @author Jing Dong
%   perform a single iteration of uniform search
%   can be used for uniform search, bi-directional search, tri-directional
%   search


% remove this node from frontier, add in explored
current_route = route_list{lowest_cost_idx};
current_node_cost = frontier_dist_list(lowest_cost_idx);
explored_list = [explored_list lowest_cost_node];
%     explored_route_list = [explored_route_list route_list(1, lowest_cost_idx)];

frontier_node_list(lowest_cost_idx) = [];
frontier_dist_list(lowest_cost_idx) = [];
route_list(:, lowest_cost_idx) = [];

% expand the tree node of current selected node, put in frontier
child_list = graph.connect_dirct{lowest_cost_node};
for i=1:numel(child_list)
    % only accpet child not in explored
    if sum(find(explored_list == child_list(i))) == 0
        frontier_node_list = [frontier_node_list  child_list(i)];
        frontier_dist_list = [frontier_dist_list  current_node_cost + ...
            graph.dist_mat(lowest_cost_node, child_list(i))];
        route_list = [route_list [current_route child_list(i)]];
    end
end

% jump out if out of frontier
if numel(frontier_node_list) == 0
    return
end

% find lowest cost in current frontier node to expand
lowest_cost_idx = find(frontier_dist_list == min(frontier_dist_list));
lowest_cost_idx = lowest_cost_idx(1);
lowest_cost_node = frontier_node_list(lowest_cost_idx);

end

