function result = uniformSearch(graph, root, goal)
%UNIFORMSEARCH Uniform cost search, call usearchEngine
%   @author Jing Dong
% resut format:
%   is_found: 1 means found, 0 means not found
%   cost: route distance
%   route: extract route node index
%   explored_node: searched node

% init
frontier_node_list = [root];
frontier_dist_list = [0];

route_list = cell(1,1);
route_list{1} = [root];

explored_list = [];
% explored_route_list = cell(0);

lowest_cost_idx = 1;
lowest_cost_node = root;

% loop
while ~(numel(frontier_node_list) == 0 || lowest_cost_node == goal)
    
    % call usearchEngine
    [frontier_node_list, frontier_dist_list, route_list, ...
        explored_list, lowest_cost_idx, lowest_cost_node] = ...
        usearchEngine(frontier_node_list, frontier_dist_list, route_list, ...
        explored_list, lowest_cost_idx, lowest_cost_node, graph);
    
%     disp('=========================')
end

% check whether frontier is empty: empty means cannot reach goal
if numel(frontier_node_list) == 0
    result.is_found = 0;
    return
end

% check whether goal is reached
if lowest_cost_node == goal
    result.is_found = 1;
    result.cost = frontier_dist_list(lowest_cost_idx);
    result.route = route_list{lowest_cost_idx};
    result.explored_node = explored_list;
%     result.explored_route = explored_route_list;
    return
end

end

