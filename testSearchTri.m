% test searching for 3 nodes
% @author Jing Dong

clear
close all
includeDependencies;
addpath('./data')

% % unit test data
% unittest_data;
% graph = graph_test;
% % given index
% node_idx1 = 1
% node_idx2 = 5
% node_idx3 = 3

% =========================================================================
% load from OSM, it's pretty slow. If you want speed up, just comment out
% this section and load data/atlanta_graph.mat
openstreetmap_filename = 'atlanta.osm';
disp('loading OSM XML file ...')
[parsed_osm, ~] = parse_openstreetmap(openstreetmap_filename);
% parse data to graph format
ref_point = [-84.37 33.79];
graph = parseOSM2Graph(parsed_osm, ref_point);

% % =========================================================================
% % use this section to load pre-cached graph data
% % atlanta data
% % node: 12240
% load('data/atlanta_graph.mat');


% given index
% node_idx1 = ceil(12240*rand(1))
% node_idx2 = ceil(12240*rand(1))
% node_idx3 = ceil(12240*rand(1))
node_idx1 = 9796
node_idx2 = 1737
node_idx3 = 2345


% =========================================================================
% tri-search
result = triDirctSearch(graph, node_idx1, node_idx2, node_idx3)

% plot map
disp('ploting map ...')
figure(2)
axis equal, grid
plotGraph(graph, 2);

if result.is_found
    % plot explored node
    plotRoute(graph, result.explored_node{1}, 2, 'g.', 'MarkerSize', 10);
    plotRoute(graph, result.explored_node{2}, 2, 'c.', 'MarkerSize', 10);
    plotRoute(graph, result.explored_node{3}, 2, 'y.', 'MarkerSize', 10);
    plotRoute(graph, result.route{1}, 2, 'r-', 'LineWidth', 3);
    plotRoute(graph, result.route{2}, 2, 'm-', 'LineWidth', 3);
else
    plotRoute(graph, [node_idx1], 2, 'r.', 'MarkerSize', 20);
    plotRoute(graph, [node_idx2], 2, 'r.', 'MarkerSize', 20);
    plotRoute(graph, [node_idx3], 2, 'r.', 'MarkerSize', 20);
end