CS 6601 Homework 1: Searching
--------
MATLAB implementations of uniform cost search/bi-directinoal search/tri-directinoal search.

Usage
--------
1. if you want to run 3 city problem and check # of visited nodes, run script test3city.m

2. if you want to test uniform cost search or bi-directional search, run script testSearch.m

3. if you want to test tri-directional search, run script testSearchTri.m

4. speed up: loading OSM file is pretty slow (~several minutes), so we have cached graph data file atlanta_graph.mat in data folder. If you want to skip the super slow OSM file loading, just comment them out and load atlanta_graph.mat file

Dependencies (code by others)
---------
Note: all dependencies have been included in the code so no dependency needed to be install before running the code. You can directly run the code in MATLAB

- [OpenStreetMap Functions](http://www.mathworks.com/matlabcentral/fileexchange/35819-openstreetmap-functions) Used to parse OSM XML file to matlab strcuture.
