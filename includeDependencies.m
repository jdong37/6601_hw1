function includeDependencies
%INCLUDEDEPENDENCIES Include all dependencies needed for osm_matlab
%   @author Jing Dong

addpath('./osm_matlab');
addpath('./osm_matlab/dependencies/xml2struct');
addpath('./osm_matlab/dependencies/lat_lon_proportions');
addpath('./osm_matlab/dependencies/gaimc');
addpath('./osm_matlab/dependencies/plotmd');
addpath('./osm_matlab/dependencies/textmd');
addpath('./osm_matlab/dependencies/hold');

end

