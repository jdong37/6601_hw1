function is_common_node = findCommon(node_list_a, node_list_b)
%FINDCOMMON find common node in node list
%   @author Jing Dong
%   used to find common frontier node in bi- or tri- directional search

is_common_node = 0;
for i=1:numel(node_list_a)
    if sum(find(node_list_a(i) == node_list_b)) ~= 0
        is_common_node = 1;
        break;
    end
end

end

