function result = triDirctSearch(graph, node1, node2, node3)
%TRIDIRCTSEARCH Tri-Direction Search for three nodes 
%   @author Jing Dong

% init: 3 nodes
frontier_node_1 = [node1];
frontier_dist_1 = [0];
frontier_node_2 = [node2];
frontier_dist_2 = [0];
frontier_node_3 = [node3];
frontier_dist_3 = [0];

route_1 = cell(1,1);
route_1{1} = [node1];
route_2 = cell(1,1);
route_2{1} = [node2];
route_3 = cell(1,1);
route_3{1} = [node3];

explored_1 = [];
explored_2 = [];
explored_3 = [];

lowest_cost_idx_1 = 1;
lowest_cost_node_1 = node1;
lowest_cost_idx_2 = 1;
lowest_cost_node_2 = node2;
lowest_cost_idx_3 = 1;
lowest_cost_node_3 = node3;

% stopping conditions
found_route_iter = [0 0 0];
found_route_count = 0;

% loop
while found_route_count < 2 
    
    % call usearchEngine: forward and bakcward
    [frontier_node_1, frontier_dist_1, route_1, ...
        explored_1, lowest_cost_idx_1, lowest_cost_node_1] = ...
        usearchEngine(frontier_node_1, frontier_dist_1, route_1, ...
        explored_1, lowest_cost_idx_1, lowest_cost_node_1, graph);

    [frontier_node_2, frontier_dist_2, route_2, ...
        explored_2, lowest_cost_idx_2, lowest_cost_node_2] = ...
        usearchEngine(frontier_node_2, frontier_dist_2, route_2, ...
        explored_2, lowest_cost_idx_2, lowest_cost_node_2, graph);
    
    [frontier_node_3, frontier_dist_3, route_3, ...
        explored_3, lowest_cost_idx_3, lowest_cost_node_3] = ...
        usearchEngine(frontier_node_3, frontier_dist_3, route_3, ...
        explored_3, lowest_cost_idx_3, lowest_cost_node_3, graph);

    % check whether frontier is empty: empty means cannot reach goal
    if numel(frontier_node_1) == 0 || numel(frontier_node_2) == 0 || ...
            numel(frontier_node_3) == 0
        result.is_found = 0;
        return
    end
    
    % check whether 3 sides meet each other
    if findCommon(frontier_node_1, frontier_node_2) && found_route_iter(1) == 0
        % 1 meet 2
        % get closet route
        [min_cost_1idx min_cost_2idx] = findClosestRoute(frontier_node_1, ...
            frontier_dist_1, frontier_node_2, frontier_dist_2);
        % get result
        found_route{found_route_count + 1}.cost = ...
            frontier_dist_1(min_cost_1idx) + frontier_dist_2(min_cost_2idx);
        found_route{found_route_count + 1}.route = ...
            [route_1{min_cost_1idx} wrev(route_2{min_cost_2idx})];
        found_route{found_route_count + 1}.expl_cache{1} = explored_1;
        found_route{found_route_count + 1}.expl_cache{2} = explored_2;
        % counter
        found_route_iter(1) = 1;
        found_route_count = found_route_count + 1;
    end
    
    if findCommon(frontier_node_1, frontier_node_3) && found_route_iter(2) == 0
        % 1 meet 3
        % get closet route
        [min_cost_1idx min_cost_3idx] = findClosestRoute(frontier_node_1, ...
            frontier_dist_1, frontier_node_3, frontier_dist_3);
        % get result
        found_route{found_route_count + 1}.cost = ...
            frontier_dist_1(min_cost_1idx) + frontier_dist_3(min_cost_3idx);
        found_route{found_route_count + 1}.route = ...
            [route_1{min_cost_1idx} wrev(route_3{min_cost_3idx})];
        found_route{found_route_count + 1}.expl_cache{1} = explored_1;
        found_route{found_route_count + 1}.expl_cache{3} = explored_3;
        % counter
        found_route_iter(2) = 1;
        found_route_count = found_route_count + 1;
    end
    
    if findCommon(frontier_node_2, frontier_node_3) && found_route_iter(3) == 0
        % 1 meet 2
        % get closet route
        [min_cost_2idx min_cost_3idx] = findClosestRoute(frontier_node_2, ...
            frontier_dist_2, frontier_node_3, frontier_dist_3);
        % get result
        found_route{found_route_count + 1}.cost = ...
            frontier_dist_2(min_cost_2idx) + frontier_dist_3(min_cost_3idx);
        found_route{found_route_count + 1}.route = ...
            [route_2{min_cost_2idx} wrev(route_3{min_cost_3idx})];
        found_route{found_route_count + 1}.expl_cache{2} = explored_2;
        found_route{found_route_count + 1}.expl_cache{3} = explored_3;
        % counter
        found_route_iter(3) = 1;
        found_route_count = found_route_count + 1;
    end
end

% get final result ready
result.is_found = 1;
result.cost = found_route{1}.cost + found_route{2}.cost;
result.route{1} = found_route{1}.route;
result.route{2} = found_route{2}.route;

% range expl_cache size 
for i=1:2
    if numel(found_route{i}.expl_cache) < 2
        found_route{i}.expl_cache{2} = [];
    end
    if numel(found_route{i}.expl_cache) < 3
        found_route{i}.expl_cache{3} = [];
    end
end

size_cache1 = numel(found_route{1}.expl_cache{1});
size_cache2 = numel(found_route{2}.expl_cache{1});
max_cache_idx = find(max([size_cache1 size_cache2]) == [size_cache1 size_cache2]);
max_cache_idx = max_cache_idx(1);
result.explored_node{1} = found_route{max_cache_idx}.expl_cache{1};

size_cache1 = numel(found_route{1}.expl_cache{2});
size_cache2 = numel(found_route{2}.expl_cache{2});
max_cache_idx = find(max([size_cache1 size_cache2]) == [size_cache1 size_cache2]);
max_cache_idx = max_cache_idx(1);
result.explored_node{2} = found_route{max_cache_idx}.expl_cache{2};

size_cache1 = numel(found_route{1}.expl_cache{3});
size_cache2 = numel(found_route{2}.expl_cache{3});
max_cache_idx = find(max([size_cache1 size_cache2]) == [size_cache1 size_cache2]);
max_cache_idx = max_cache_idx(1);
result.explored_node{3} = found_route{max_cache_idx}.expl_cache{3};

end

