% test 3 city of 100 trials
% @author Jing Dong

clear
close all
includeDependencies;
addpath('./data')

% =========================================================================
% load from OSM, it's pretty slow. If you want speed up, just comment out
% this section and load data/atlanta_graph.mat
openstreetmap_filename = 'atlanta.osm';
disp('loading OSM XML file ...')
[parsed_osm, ~] = parse_openstreetmap(openstreetmap_filename);
% parse data to graph format
ref_point = [-84.37 33.79];
graph = parseOSM2Graph(parsed_osm, ref_point);

% % =========================================================================
% % use this section to load pre-cached graph data
% % atlanta data
% % node: 12240
% load('data/atlanta_graph.mat');


num_try = 100;
count = 1;
for count=1:num_try
    
    fprintf('count : %d\n\n', count)
    % given index
    node_idx1 = ceil(12240*rand(1));
    node_idx2 = ceil(12240*rand(1));
    node_idx3 = ceil(12240*rand(1));
    
    % =========================================================================
    % uni-searching
    result_uni{1} = uniformSearch(graph, node_idx1, node_idx2);
    result_uni{2} = uniformSearch(graph, node_idx1, node_idx3);
    result_uni{3} = uniformSearch(graph, node_idx2, node_idx3);
    
    % cannot reach
    if ~(result_uni{1}.is_found && result_uni{2}.is_found && result_uni{3}.is_found)
        fprintf('count : %d :: Not connected\n', count)
        result_uni_collect{count}.is_connect = 0;
        continue
    end
    % get 2 route
    [~, chose_idx] = sort([result_uni{1}.cost, result_uni{2}.cost, result_uni{3}.cost]);
    chose_idx = chose_idx(1:2);
    
    % put in result
    result_uni_collect{count}.is_connect = 1;
    result_uni_collect{count}.cost = result_uni{chose_idx(1)}.cost + ...
        result_uni{chose_idx(2)}.cost;
    result_uni_collect{count}.explored = numel(result_uni{1}.explored_node) + ...
        numel(result_uni{2}.explored_node) + numel(result_uni{3}.explored_node);
    result_uni_collect{count}.result = result_uni;
    
    
    % counter
    fprintf('uni-directional ')
    result_uni_collect{count}
    disp('--------------------------------------------------------------')
    
    
    % =========================================================================
    % bi-searching
    result_bi{1} = biDirctSearch(graph, node_idx1, node_idx2);
    result_bi{2} = biDirctSearch(graph, node_idx1, node_idx3);
    result_bi{3} = biDirctSearch(graph, node_idx2, node_idx3);
    
    % cannot reach
    if ~(result_bi{1}.is_found && result_bi{2}.is_found && result_bi{3}.is_found)
        fprintf('count : %d :: Not connected\n', count)
        result_bi_collect{count}.is_connect = 0;
        continue
    end
    % get 2 route
    [~, chose_idx] = sort([result_bi{1}.cost, result_bi{2}.cost, result_bi{3}.cost]);
    chose_idx = chose_idx(1:2);
    
    % put in result
    result_bi_collect{count}.is_connect = 1;
    result_bi_collect{count}.cost = result_bi{chose_idx(1)}.cost + ...
        result_bi{chose_idx(2)}.cost;
    result_bi_collect{count}.explored = ...
        numel(result_bi{1}.explored_node{1}) + numel(result_bi{1}.explored_node{2}) + ...
        numel(result_bi{2}.explored_node{1}) + numel(result_bi{2}.explored_node{2}) + ...
        numel(result_bi{3}.explored_node{1}) + numel(result_bi{3}.explored_node{2});
    result_bi_collect{count}.result = result_bi;
    
    % counter
    fprintf('bi-directional ')
    result_bi_collect{count}
    disp('--------------------------------------------------------------')
    
    
    % =========================================================================
    % tri-searching
    result_tri = triDirctSearch(graph, node_idx1, node_idx2, node_idx3);
    
    % cannot reach
    if ~result_tri.is_found
        fprintf('count : %d :: Not connected\n', count)
        result_tri_collect{count}.is_connect = 0;
        continue
    end
    
    % put in result
    result_tri_collect{count}.is_connect = 1;
    result_tri_collect{count}.cost = result_tri.cost;
    result_tri_collect{count}.explored = ...
        numel(result_tri.explored_node{1}) + numel(result_tri.explored_node{2}) + ...
        numel(result_tri.explored_node{3});
    result_tri_collect{count}.result = result_tri;
    
    % counter
    fprintf('tri-directional ')
    result_tri_collect{count}
    disp('--------------------------------------------------------------')
   
    
    disp('====================================================================')
end

% =========================================================================
% explain the data

connect_count = 0;
node_uni = 0; 
node_bi = 0; 
node_tri = 0;
for i=1:num_try
    if numel(result_uni_collect{i}) && numel(result_bi_collect{i}) && ...
        numel(result_tri_collect{i})
    
    % connected
    connect_count = connect_count + 1;
    
    node_uni = node_uni + result_uni_collect{i}.explored;
    node_bi = node_bi + result_bi_collect{i}.explored;
    node_tri = node_tri + result_tri_collect{i}.explored;
    
    end
end
node_uni = node_uni/connect_count
node_bi = node_bi/connect_count
node_tri = node_tri/connect_count

labell = {'Uni-search';'Bi-search';'Tri-search'};
bar([node_uni node_bi node_tri]);
set(gca,'XTickLabel', labell);